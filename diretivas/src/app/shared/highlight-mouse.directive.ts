import { Directive, HostListener, ElementRef, Renderer2, HostBinding } from '@angular/core';

@Directive({
  selector: '[appHighlightMouse]'
})
export class HighlightMouseDirective {

  @HostBinding('style.backgroundColor') backgroundColor: string;

  constructor(
    /*private elementref: ElementRef,
    private renderer: Renderer2,*/
  ) { }
  @HostListener('mouseenter')
  onMouseOver(): void {
    // this.renderer.setStyle(this.elementref.nativeElement, 'background-color', 'red');
    this.backgroundColor = 'red';
  }

  @HostListener('mouseleave')
  onMouseOut(): void {
    // this.renderer.setStyle(this.elementref.nativeElement, 'background-color', 'white');
    this.backgroundColor = 'white';
  }
}


import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AlunosService {

  private alunos: any[] = [
    {id: 1, nome: 'Aluno 01', email: 'Aluno01@email.com'},
    {id: 2, nome: 'Aluno 02', email: 'Aluno02@email.com'},
    {id: 3, nome: 'Aluno 03', email: 'Aluno03@email.com'},
  ];

  constructor() { }

  index() {
    return this.alunos;
  }

  show (id:number) {
    let alunos = this.index();
    for (let i = 0; i < alunos.length; i++) {
      let aluno = alunos[i];
      if (aluno.id == id) {
        return aluno;
      }
    }
    return null;
  }
}

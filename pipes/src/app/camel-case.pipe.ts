import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'camelCase'
})
export class CamelCasePipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {

    let values = value.split(' ');
    let result = '';

    for (let v of values) {
      result += this.capitalize(v) + ' ';
    }

    return result;
  }
  capitalize (letter:string): string {
    return letter.substr(0,1).toUpperCase() +
    letter.substr(1).toLowerCase();
  }
}

import {Component, OnDestroy, OnInit} from '@angular/core';
import {CursosService} from "./cursos.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";

@Component({
  selector: 'rotas-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.css']
})
export class CursosComponent implements OnInit, OnDestroy {

  cursos: any[];
  pagina: number;
  inscricao: Subscription;

  constructor(
      private service: CursosService,
      private route: ActivatedRoute,
      private router: Router,
  ) { }

  ngOnInit(): void {
    this.cursos = this.service.getCursos();
    this.inscricao = this.route.queryParams.subscribe(
        (queryParams: any) => {
          this.pagina = queryParams.pagina
        }
    )}

  ngOnDestroy(){
    this.inscricao.unsubscribe();
  }
  nextPage() {
    this.router.navigate(['/cursos'], {
      queryParams: {
        pagina: ++this.pagina
      }
    });
  }
}

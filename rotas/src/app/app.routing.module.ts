import {RouterModule, Routes} from "@angular/router";
import {LoginComponent} from "./login/login.component";
import {HomeComponent} from "./home/home.component";
import {NgModule} from "@angular/core";
import {CursoDetalheComponent} from "./cursos/curso-detalhe/curso-detalhe.component";
import {CursoNaoEncontradoComponent} from "./cursos/curso-nao-encontrado/curso-nao-encontrado.component";

const appRoutes: Routes = [
    { path: 'cursos', loadChildren: () => import('./cursos/cursos.module').then(module => module.CursosModule)},
    { path: 'alunos', loadChildren: () => import('./alunos/alunos.module').then(module => module.AlunosModule)},
    // { path: 'cursos', loadChildren: './app/cursos/cursos.module#CursosModule' },
    // { path: 'curso/:id',component: CursoDetalheComponent },
    { path: 'login',component: LoginComponent },
    { path: '',component: HomeComponent },
    // { path: 'naoEncontrado',component: CursoNaoEncontradoComponent }
]

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule
    ],
})
export class AppRoutingModule {

}

import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import {AppRoutingModule} from "./app.routing.module";
import {FormsModule} from "@angular/forms";
// import {CursosModule} from "./cursos/cursos.module";
// import {routing} from "./app.routing";
// import { CursosComponent } from './cursos/cursos.component';
// import {CursosService} from "./cursos/cursos.service";
// import {CursoDetalheComponent} from "./cursos/curso-detalhe/curso-detalhe.component";
// import {CursoNaoEncontradoComponent} from "./cursos/curso-nao-encontrado/curso-nao-encontrado.component";
// import {AlunosModule} from "./alunos/alunos.module";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    //CursosComponent,
    //CursoDetalheComponent,
    //CursoNaoEncontradoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    // CursosModule,
    AppRoutingModule,
    // AlunosModule,
    //routing
  ],
  providers: [
    // CursosService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }

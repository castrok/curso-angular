import { Directive, Input,
  TemplateRef /*REFERENCIA O <template> */,
  ViewContainerRef /*REFERENCIA O CONTEUDO A SER RENDERIZADO EM <atemplate> */
} from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[ngElse]'
})
export class NgElseDirective {

  @Input()
  set ngElse(condition: boolean) {
    if (!condition) {
      // renderiza o template numa diretiva estrutural
      this.viewcontainerref.createEmbeddedView(this.templateref);
    } else {
      this.viewcontainerref.clear();
    }
  }

  constructor(
    /* Como essa diretiva poderá ser utilizada em qualquer tag utilizamos o <any> */
    private templateref: TemplateRef<any>,
    private viewcontainerref: ViewContainerRef
  ) { }
}

import {Directive, HostBinding, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  @Input()
  private defaultColor:string = 'white';

  @Input('highlight')
  private highlightColor:string = 'yellow';

  @HostBinding('style.backgroundColor')
  backgroundColor: string;

  constructor() { }

  ngOnInit(): void {
    this.backgroundColor = this.defaultColor;
  }

  @HostListener('mouseenter')
  onMouseOver(): void {
    this.backgroundColor = this.highlightColor;
  }

  @HostListener('mouseleave')
  onMouseOut(): void {
    this.backgroundColor = this.defaultColor;
  }
}

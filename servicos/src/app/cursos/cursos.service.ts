import {EventEmitter, Injectable} from "@angular/core";

@Injectable()
export class CursosService {

    // emitirCursoCriado = new EventEmitter<string>();
    static criouNovoCurso = new EventEmitter<string>();
    private cursos: string[] = ['Angular 2', 'Java', 'Phone Gap'];

    constructor() {
        console.log('CursosService');
    }

    getCursos() {
        return this.cursos;
    }

    addCurso (curso: string) {
        this.cursos.push(curso);
        CursosService.criouNovoCurso.emit(curso);
    }
}

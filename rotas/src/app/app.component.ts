import { Component } from '@angular/core';

@Component({
  selector: 'rotas-root',
  templateUrl: 'app.component.html',
  styles: []
})
export class AppComponent {
  title = 'rotas';
}

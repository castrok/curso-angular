import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {AlunosService} from "../alunos.service";

@Component({
  selector: 'rotas-aluno-detalhe',
  templateUrl: './aluno-detalhe.component.html',
  styleUrls: ['./aluno-detalhe.component.css']
})
export class AlunoDetalheComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  public aluno: any;

  constructor(
      private alunoService: AlunosService,
      private route: ActivatedRoute,
      private router: Router
  ) { }

  ngOnInit(): void {
    this.subscription = this.route.params.subscribe((params:any) => {
      let id = params['id'];

      this.aluno = this.alunoService.show(id);

      if (this.aluno == null) {
        alert("404");
      }
    });
  }

  ngOnDestroy () {
    this.subscription.unsubscribe();
  }

  editarAluno() {
    this.router.navigate(['/alunos', this.aluno.id, 'edit']);
  }
}

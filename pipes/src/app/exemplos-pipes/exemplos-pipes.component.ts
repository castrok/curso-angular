import { Component, OnInit } from '@angular/core';
import {interval, Observable} from 'rxjs';
import {map} from "rxjs/operators";

@Component({
  selector: 'app-exemplos-pipes',
  templateUrl: './exemplos-pipes.component.html',
  styleUrls: ['./exemplos-pipes.component.css']
})

export class ExemplosPipesComponent implements OnInit {

  livro: any = {
    titulo: 'Learning JavaScript Data Structures and Algorithms 2nd ed',
    rating: 4.54321,
    numeroPaginas: 314,
    preco: 44.99,
    dataLancamento: new Date(2016, 5, 23),
    url: 'http://a.co/glqjpRP'
  };

  books: string[] = ['Angular 2', 'Java'];
  filtro:string;

  valorAsync = new Promise((resolve, reject) => {
    setTimeout(() => resolve("Valor assíncrono"), 2000);
  });

  valorAsync2 = interval(2000).pipe(map(valor => 'Valor assíncrono 2'));

  constructor() { }

  ngOnInit(): void {
  }

  addCurso(valor: string): void {
    if (valor.length) {
      this.books.push(valor);
      console.log(this.books);
    }
  }

  obterCursos() {
    if (this.books.length === 0 || this.filtro === undefined || this.filtro.trim() === '') {
      return this.books;
    }

    return this.books.filter((v) => {
      if (v.toLowerCase().indexOf(this.filtro.toLowerCase()) >= 0) {
        return true;
      }
      return false;
    });
  }
}

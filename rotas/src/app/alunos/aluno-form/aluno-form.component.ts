import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AlunosService} from "../alunos.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'rotas-aluno-form',
  templateUrl: './aluno-form.component.html',
  styleUrls: ['./aluno-form.component.css']
})
export class AlunoFormComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  public aluno: any;

  constructor(
      private route: ActivatedRoute,
      private alunoService: AlunosService
  ) { }

  ngOnInit(): void {
    this.subscription = this.route.params.subscribe((params:any) => {
      let id = params['id'];

      this.aluno = this.alunoService.show(id);

      if (this.aluno === null) {
        this.aluno = {};
      }
    });
  }

  ngOnDestroy () {
    this.subscription.unsubscribe();
  }

}

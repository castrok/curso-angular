import { Component, OnInit } from '@angular/core';
import { CursosService } from './cursos.service';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.css'],
  providers: [CursosService]
})
export class CursosComponent implements OnInit {

  cursos: string[] = [];
  // cursoservice: CursosService;

  constructor(private cursoservice: CursosService) {
    //this.cursoservice = new CursosService();
    //this.cursoservice = cursoService;
  }

  ngOnInit(): void {
    this.cursos = this.cursoservice.getCursos();

    CursosService.criouNovoCurso.subscribe(
        // curso => console.log(curso)
        curso => this.cursos.push(curso)
    );
  }
}
